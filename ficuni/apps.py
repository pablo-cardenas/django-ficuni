from django.apps import AppConfig


class FicuniConfig(AppConfig):
    name = 'ficuni'
