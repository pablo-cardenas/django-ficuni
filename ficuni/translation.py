from modeltranslation.translator import register, TranslationOptions
from .models import (Course, Department, Category, Condition, Dedication,
                     Teacher, HourType)


@register(Course)
class CourseTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Department)
class DepartmentTranslationOptions(TranslationOptions):
    fields = ('name', 'summary', 'organization')


@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Condition)
class ConditionTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Dedication)
class DedicationTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Teacher)
class TeacherTranslationOptions(TranslationOptions):
    fields = ('summary',)


@register(HourType)
class HourTypeTranslationOpetions(TranslationOptions):
    fields = ('name',)
