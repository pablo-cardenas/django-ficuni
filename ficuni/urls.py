from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.urls import path, include
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from rest_framework.routers import DefaultRouter

from .views import (CycleCourseList, DepartmentElectiveList, CourseDetail,
                    DepartmentList, DepartmentDetail, TeacherDetail,
                    NewsletterDetail, CourseViewSet, TeacherViewSet,
                    LocationViewSet, IndexView, AnnouncementDetail)


app_name = 'ficuni'

router = DefaultRouter()
router.register('teachers', TeacherViewSet)
router.register('courses', CourseViewSet)
router.register('locations', LocationViewSet, 'Location')


urlpatterns = [
    path('api/', include((router.urls, 'api'), 'instance_name')),
    path('', IndexView.as_view(), name='index'),
    path('courses/ciclo-<int:cycle>/', CycleCourseList.as_view(),
         name='course_by_cycle'),
    path('courses/electivos/', DepartmentElectiveList.as_view(),
         name='electives_by_department'),
    path('course/<slug:code>/', CourseDetail.as_view(),
         name='course_detail'),
    path(_('departments/'), DepartmentList.as_view(),
         name='department_list'),
    path(_('department/<slug:acronym>/'), DepartmentDetail.as_view(),
         name='department_detail'),
    path('teacher/<int:pk>/', TeacherDetail.as_view(),
         name='teacher_detail'),
    path('cruce-de-horarios/',
         TemplateView.as_view(template_name="ficuni/cruce_horarios.html"),
         name='cruce_horarios'),
    path('deanery/',
         TemplateView.as_view(template_name="ficuni/deanery.html"),
         name='deanery'),
    url(r'^compartir-horario/.*',
        TemplateView.as_view(template_name="ficuni/share_schedule.html"),
        name='share_schedule'),
    path('newsletter/<slug:newsletter_type>/<slug:code>/',
         NewsletterDetail.as_view(),
         name='newsletter_detail'),
    path('announcement/<slug:code>/',
         AnnouncementDetail.as_view(),
         name='announcement_detail'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
