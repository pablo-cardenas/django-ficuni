from django.db.models import Q, Count
from django.db.models.functions import TruncMonth
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import ListView, DetailView, TemplateView
from rest_framework import viewsets
from itertools import groupby

from .models import Course, Cycle, Department, Teacher, Location, Newsletter, Announcement
from . import serializers


class IndexView(TemplateView):
    template_name = 'ficuni/index.html'
    context_object_name = 'context'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['newsletter_list'] = Newsletter.objects.order_by('-pub_date')[:8]
        context['announcement_list'] = Announcement.objects.order_by('-pub_date')[:8]
        return context


class CycleCourseList(ListView):
    template_name = 'ficuni/courses_by_cycle.html'
    context_object_name = 'course_list'

    def get_queryset(self):
        self.cycle = get_object_or_404(Cycle, pk=self.kwargs['cycle'])
        return Course.objects.filter(cycle=self.cycle)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_cycle'] = self.cycle
        context['cycle_list'] = Cycle.objects.all()
        return context

    def dispatch(self, request, *args, **kwargs):
        if self.kwargs['cycle'] == 11:
            return redirect('ficuni:electives_by_department')
        return super().dispatch(request, *args, **kwargs)


class DepartmentElectiveList(ListView):
    template_name = 'ficuni/electives_by_department.html'
    context_object_name = 'course_list'

    def get_queryset(self):
        self.cycle = get_object_or_404(Cycle, pk=11)
        return Course.objects.filter(cycle=self.cycle)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_cycle'] = self.cycle
        context['cycle_list'] = Cycle.objects.all()
        context['department_list'] = Department.objects.all()
        return context


class CourseDetail(DetailView):
    model = Course
    context_object_name = 'course'
    slug_field = slug_url_kwarg = 'code'


class DepartmentList(ListView):
    model = Department
    context_object_name = 'department_list'


class DepartmentDetail(DetailView):
    model = Department
    context_object_name = 'department'
    slug_field = slug_url_kwarg = 'acronym'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['department_list'] = Department.objects.all()
        return context


class TeacherDetail(DetailView):
    model = Teacher
    context_object_name = 'teacher'
    # slug_field = slug_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        teacher = context['teacher']
        context['section_list'] = {location.section for location in
                                   teacher.location_set.all()}
        return context


class NewsletterDetail(DetailView):
    model = Newsletter
    context_object_name = 'newsletter'

    def get_object(self):
        code = self.kwargs.get('code')
        newsletter_type = self.kwargs.get('newsletter_type')
        queryset = Newsletter.objects.filter(newsletter_type__slug=newsletter_type)
        return get_object_or_404(queryset, code=code)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        newsletter_list = groupby(
            Newsletter.objects.annotate(month=TruncMonth('pub_date')),
            key=lambda n: n.month)
        context['newsletter_list'] = [(k, list(g)) for k, g in newsletter_list]
        return context


class AnnouncementDetail(DetailView):
    model = Announcement
    context_object_name = 'announcement'
    slug_field = slug_url_kwarg = 'code'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        announcement_list = groupby(
            Announcement.objects.annotate(month=TruncMonth('pub_date')),
            key=lambda n: n.month)
        context['announcement_list'] = [(k, list(g)) for k, g in announcement_list]
        return context


class CourseViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Course.objects.all()
    serializer_class = serializers.CourseSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.CourseSerializer
        if self.action == 'retrieve':
            return serializers.CourseDetailSerializer


class TeacherViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Teacher.objects.all()
    serializer_class = serializers.TeacherDetailSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.TeacherSerializer
        if self.action == 'retrieve':
            return serializers.TeacherDetailSerializer


class LocationViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.LocationSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """

        q = Q()
        for key in self.request.query_params:
            q |= Q(
                section__course__code=key,
                section__section=self.request.query_params.get(key, None)
            )

        print(q)
        return Location.objects.filter(q)
