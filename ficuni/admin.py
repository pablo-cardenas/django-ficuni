from django.contrib import admin
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from django_summernote.admin import SummernoteModelAdmin
from modeltranslation.admin import TranslationAdmin
import xlsxwriter
import io

from . import models


class SectionInline(admin.TabularInline):
    model = models.Section
    extra = 0


class LocationInline(admin.TabularInline):
    model = models.Location
    extra = 0


@admin.register(models.Section)
class SectionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'course_code', 'section', 'course_name')
    list_filter = ('course__department',)
    search_fields = ('course__name',)
    inlines = (LocationInline,)

    def course_name(self, obj):
        return obj.course.name
    course_name.admin_order_field = 'course__name'
    course_name.short_description = _("course's name")

    def course_code(self, obj):
        return obj.course.code
    course_code.admin_order_field = 'course__code'
    course_code.short_description = _("course's code")

    actions = ["export_as_excel"]

    def export_as_excel(self, request, queryset):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()

        center = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'border': 1})
        right = workbook.add_format({'align': 'right', 'valign': 'vcenter', 'border': 1})
        text_wrap = workbook.add_format({'valign': 'vcenter', 'text_wrap': True, 'border': 1})

        worksheet.merge_range(0, 0, 1, 0, 'Nº')
        worksheet.merge_range(0, 1, 1, 1, 'CURSO')
        worksheet.merge_range(0, 2, 0, 4, 'LOCALIZACIÓN', center)
        worksheet.write(1, 2, 'TIPO')
        worksheet.write(1, 3, 'HORA')
        worksheet.write(1, 4, 'AULA')
        worksheet.merge_range(0, 5, 1, 5, 'DOCENTE')
        worksheet.merge_range(0, 6, 1, 6, 'SIST.')

        min_height = 60
        row_num = 2
        for section_num, section in enumerate(queryset):
            row_section = row_num
            total = sum(max(location.teachers.count(),
                            location.classrooms.count()) for location in
                        section.location_set.all())
            num_locations = section.location_set.count()
            for location in section.location_set.all():
                worksheet.write(row_num, 2,
                                '{location.hour_type}'
                                .format(location=location))
                worksheet.write(row_num, 3,
                                '{week_day} '
                                '{location.begin_hour:02d}-'
                                '{location.end_hour:02d}'
                                .format(week_day=location.days_of_week[location.week_day - 1][1][:3].upper(),
                                        location=location))
                worksheet.write(row_num, 4,
                                "\n".join(['{}'.format(classroom)
                                          for classroom
                                          in location.classrooms.all()]))
                worksheet.write(row_num, 5,
                                "\n".join(['{}'.format(teacher)
                                           for teacher
                                           in location.teachers.all()]))

                count = max(location.teachers.count(),
                            location.classrooms.count())
                row_height = count * 13 + 5
                if total < 4:
                    row_height += (min_height - (13 * total + 5 * num_locations)) / num_locations
                worksheet.set_row(row_num, row_height)
                row_num += 1

            if row_num == row_section + 1:
                worksheet.write(row_section, 0,
                                '{:02d}'
                                .format(section_num))
                worksheet.write(row_section, 1,
                                "{section.course.name}\n"
                                "{section}\n"
                                "CRÉDITOS: {section.course.num_of_credits}\n"
                                "ALUMNOS: 40"
                                .format(section=section))
                worksheet.write(row_section, 6,
                                "{section.course.evaluation_system}"
                                .format(section=section))
            else:
                worksheet.merge_range(row_section, 0, row_num - 1, 0,
                                      '{:02d}'
                                      .format(section_num))
                worksheet.merge_range(row_section, 1, row_num - 1, 1,
                                      "{section.course.name}\n"
                                      "{section}\n"
                                      "CRÉDITOS: {section.course.num_of_credits}\n"
                                      "ALUMNOS: 40"
                                      .format(section=section))
                worksheet.merge_range(row_section, 6, row_num - 1, 6,
                                      "{section.course.evaluation_system}"
                                      .format(section=section))

        worksheet.set_column(0, 0, 4, center)
        worksheet.set_column(1, 1, 40, text_wrap)
        worksheet.set_column(2, 2, 4, center)
        worksheet.set_column(3, 3, 10, right)
        worksheet.set_column(4, 4, 8, center)
        worksheet.set_column(5, 5, 40, text_wrap)
        worksheet.set_column(6, 6, 4, center)

        # Close the workbook before sending the data.
        workbook.close()

        # Rewind the buffer.
        output.seek(0)

        # Set up the Http response.
        filename = 'django_simple.xlsx'
        response = HttpResponse(
            output,
            content_type='application/" \
                "vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename

        return response


@admin.register(models.Course)
class CourseAdmin(TranslationAdmin):
    list_display = ('code', 'name')
    inlines = [SectionInline]
    list_filter = ('department',)


@admin.register(models.Teacher)
class TeacherAdmin(TranslationAdmin):
    list_display = ('code', 'last_name', 'first_name')
    list_filter = ('departments',)

    actions = ["export_as_excel"]

    def export_as_excel(self, request, queryset):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()

        center = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'border': 1})
        right = workbook.add_format({'align': 'right', 'valign': 'vcenter', 'border': 1})
        text_wrap = workbook.add_format({'valign': 'vcenter', 'text_wrap': True, 'border': 1})

        worksheet.write(1, 0, 'Nº')
        worksheet.write(1, 1, 'APELLIDOS Y NOMBRES')
        worksheet.write(1, 2, 'COND')
        worksheet.write(1, 3, 'CAT. O R.E')
        worksheet.write(1, 4, 'O.H.')
        worksheet.write(1, 5, 'CÓDIGO')
        worksheet.write(1, 6, 'NOMBRE DEL CURSO')
        worksheet.write(1, 7, 'TIPO DE HORA')
        worksheet.write(1, 8, 'DÍA')
        worksheet.write(1, 9, 'AULA')
        worksheet.write(1, 10, 'CARGA LECTIVA (**)')

        min_height = 60
        row_num = 2
        for teacher_num, teacher in enumerate(queryset):
            row_teacher = row_num
            total = sum(max(location.teachers.count(),
                            location.classrooms.count()) for location in
                        teacher.location_set.all())
            num_locations = teacher.location_set.count()
            for location in teacher.location_set.all():
                worksheet.write(row_num, 5,
                                '{location.section}'
                                .format(location=location))
                worksheet.write(row_num, 6,
                                '{location.section.course.name}'
                                .format(location=location))
                worksheet.write(row_num, 7,
                                '{location.hour_type}'
                                .format(location=location))
                worksheet.write(row_num, 8,
                                '{week_day} '
                                '{location.begin_hour:02d}-'
                                '{location.end_hour:02d}'
                                .format(week_day=location.days_of_week[location.week_day - 1][1][:3].upper(),
                                        location=location))
                worksheet.write(row_num, 9,
                                "\n".join(['{}'.format(classroom)
                                          for classroom
                                          in location.classrooms.all()]))
                worksheet.write(row_num, 10,
                                '{}'
                                .format(location.end_hour - location.begin_hour))

                count = max(location.teachers.count(),
                            location.classrooms.count())
                row_height = count * 13 + 5
                if total < 4:
                    row_height += (min_height - (13 * total + 5 * num_locations)) / num_locations
                #worksheet.set_row(row_num, row_height)
                row_num += 1

            worksheet.merge_range(row_teacher, 0, row_num - 1, 0,
                                  '{:02d}'
                                  .format(teacher_num))
            worksheet.merge_range(row_teacher, 1, row_num - 1, 1,
                                  "{teacher.last_name}, {teacher.first_name}\n"
                                  "CÓDIGO: {teacher.code}"
                                  .format(teacher=teacher))
            worksheet.merge_range(row_teacher, 2, row_num - 1, 2,
                                  "{}"
                                  .format(teacher.condition.abbreviation if
                                          teacher.condition else ''))
            worksheet.merge_range(row_teacher, 3, row_num - 1, 3,
                                  "{}"
                                  .format(teacher.category.abbreviation if
                                          teacher.condition else ''))
            worksheet.merge_range(row_teacher, 4, row_num - 1, 4,
                                  "{}"
                                  .format(teacher.dedication.abbreviation if
                                          teacher.dedication else ''))

        worksheet.set_column(0, 0, 4, center)
        worksheet.set_column(1, 1, 40, text_wrap)
        worksheet.set_column(2, 2, 4, center)
        worksheet.set_column(3, 3, 4, center)
        worksheet.set_column(4, 4, 4, center)
        worksheet.set_column(5, 5, 10, text_wrap)
        worksheet.set_column(6, 6, 40, text_wrap)
        worksheet.set_column(7, 7, 4, center)
        worksheet.set_column(8, 8, 4, right)
        worksheet.set_column(8, 8, 10, right)
        worksheet.set_column(9, 9, 10, center)
        worksheet.set_column(10, 10, 4, center)

        # Close the workbook before sending the data.
        workbook.close()

        # Rewind the buffer.
        output.seek(0)

        # Set up the Http response.
        filename = 'django_simple.xlsx'
        response = HttpResponse(
            output,
            content_type='application/" \
                "vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename

        return response


@admin.register(models.Department)
class DepartmentAdmin(TranslationAdmin, SummernoteModelAdmin):
    summernote_fields = ('organization',)


@admin.register(models.Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    list_display = ('code', 'newsletter_type')
    list_filter = ('newsletter_type__name',)


@admin.register(models.Announcement)
class AnnouncementAdmin(admin.ModelAdmin):
    list_display = ('code',)


admin.site.register(models.Category, TranslationAdmin)
admin.site.register(models.Classroom)
admin.site.register(models.Dedication, TranslationAdmin)
admin.site.register(models.EvaluationSystem)
admin.site.register(models.Grade)
admin.site.register(models.HourType, TranslationAdmin)

admin.site.site_header = "FIC-UNI"
