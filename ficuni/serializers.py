from rest_framework import serializers

from .models import Course, Section, EvaluationSystem, Location, Teacher,\
                    Classroom, HourType, Department


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ('acronym',)


class HourTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = HourType
        fields = ('abbreviation',)


class ClassroomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Classroom
        fields = ('code',)


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ('id', 'code', 'first_name', 'last_name')


class EvaluationSystemSerializer(serializers.ModelSerializer):
    class Meta:
        model = EvaluationSystem
        fields = ('code',)


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'code', 'name', 'cycle')


class SectionSerializer(serializers.ModelSerializer):
    course = CourseSerializer(read_only=True)

    class Meta:
        model = Section
        fields = ('course', 'section', 'location_set')


class LocationSerializer(serializers.ModelSerializer):
    teachers = TeacherSerializer(many=True, read_only=True)
    classrooms = ClassroomSerializer(many=True, read_only=True)
    hour_type = HourTypeSerializer(read_only=True)
    section = SectionSerializer(read_only=True)

    class Meta:
        model = Location
        fields = ('id', 'week_day', 'begin_hour', 'end_hour', 'hour_type',
                  'classrooms', 'teachers', 'section')


class SectionDetailSerializer(serializers.ModelSerializer):
    location_set = LocationSerializer(many=True, read_only=True)
    course = CourseSerializer(read_only=True)

    class Meta:
        model = Section
        fields = ('course', 'section', 'location_set')


class CourseDetailSerializer(serializers.ModelSerializer):
    section_set = SectionDetailSerializer(many=True, read_only=True)
    evaluation_system = EvaluationSystemSerializer(read_only=True)
    department = DepartmentSerializer(read_only=True)

    class Meta:
        model = Course
        fields = ('id', 'code', 'name', 'cycle', 'num_of_credits',
                  'evaluation_system', 'department', 'section_set')


class TeacherDetailSerializer(serializers.ModelSerializer):
    location_set = LocationSerializer(many=True, read_only=True)

    class Meta:
        model = Teacher
        fields = ('id', 'code', 'first_name', 'last_name', 'location_set')
