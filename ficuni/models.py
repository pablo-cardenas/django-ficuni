from PIL import Image as Img
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from io import BytesIO
import sys

from .storages import OverwriteStorage


class Classroom(models.Model):
    code = models.SlugField(_('code'), max_length=14, unique=True)

    def __str__(self):
        return str(self.code)

    class Meta:
        verbose_name = _('classroom')
        verbose_name_plural = _('classrooms')


class EvaluationSystem(models.Model):
    code = models.SlugField(_('code'), max_length=6, unique=True)

    def __str__(self):
        return str(self.code)

    class Meta:
        verbose_name = _('evaluation system')
        verbose_name_plural = _('evaluation systems')
        ordering = ('code',)


class Department(models.Model):
    acronym = models.SlugField(_('code'), max_length=14, unique=True)
    name = models.CharField(_('name'), max_length=126)
    summary = models.TextField(_('summary'), max_length=200, blank=True,
                               default='')
    organization = models.TextField(_('organization'), blank=True, default='')

    def __str__(self):
        return str(self.acronym)

    class Meta:
        verbose_name = _('department')
        verbose_name_plural = _('departments')


class Cycle(models.Model):
    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _('cycle')
        verbose_name_plural = _('cycles')


class Course(models.Model):
    code = models.SlugField(_('code'), max_length=14, unique=True)
    name = models.CharField(_('name'), max_length=126)
    num_of_credits = models.PositiveSmallIntegerField(
        _('num. of credits'),
        validators=(
            MinValueValidator(1),
            MaxValueValidator(6),
        ))
    cycle = models.ForeignKey(
        Cycle, on_delete=models.SET_NULL, null=True,
        verbose_name=_('cycle')
    )
    evaluation_system = models.ForeignKey(
        EvaluationSystem, on_delete=models.SET_NULL, null=True,
        verbose_name=_('evaluation system')
    )
    department = models.ForeignKey(
        Department, on_delete=models.SET_NULL, null=True,
        verbose_name=_('department')
    )

    def __str__(self):
        return '{self.code}: {self.name}'.format(self=self)

    class Meta:
        verbose_name = _('course')
        verbose_name_plural = _('courses')
        ordering = ('code',)


class Section(models.Model):
    course = models.ForeignKey(
        Course, on_delete=models.CASCADE,
        verbose_name=_('course')
    )
    section = models.CharField(_('section'), max_length=6)

    def __str__(self):
        return '{self.course.code}-{self.section}'.format(self=self)

    class Meta:
        verbose_name = _('section')
        verbose_name_plural = _('sections')
        ordering = ('course',)
        unique_together = (('section', 'course'),)


class Grade(models.Model):
    abbreviation = models.SlugField(_('abbreviation'), max_length=14,
                                    unique=True)

    def __str__(self):
        return str(self.abbreviation)

    class Meta:
        verbose_name = _('grade')
        verbose_name_plural = _('grades')


class Category(models.Model):
    name = models.CharField(_('name'), max_length=30)
    abbreviation = models.SlugField(_('abbreviation'), max_length=14, unique=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')


class Condition(models.Model):
    name = models.CharField(_('name'), max_length=30)
    abbreviation = models.SlugField(_('abbreviation'), max_length=14, unique=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = _('condition')
        verbose_name_plural = _('conditions')


class Dedication(models.Model):
    name = models.CharField(_('name'), max_length=62)
    abbreviation = models.SlugField(_('abbreviation'), max_length=14, unique=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = _('dedication')
        verbose_name_plural = _('dedications')


def teacher_photo_path(teacher, filename):
    return 'teacher/{}.{}'.format(teacher.code, 'jpg')


class Teacher(models.Model):
    code = models.SlugField(_('code'), max_length=14, unique=True, blank=True,
                            null=True)
    last_name = models.CharField(_('last name'), max_length=126)
    first_name = models.CharField(_('first name'), max_length=126)
    phone = models.CharField(_('phone'), max_length=62, blank=True, null=True)
    email = models.CharField(_('email'), max_length=126, blank=True, null=True)
    summary = models.TextField(_('summary'), max_length=200, blank=True,
                               null=True)
    birthday = models.DateField(_('birthday'), blank=True, null=True)
    photo = models.ImageField(_('photo'), upload_to=teacher_photo_path,
                              storage=OverwriteStorage(), blank=True, null=True)
    grade = models.ForeignKey(
        Grade, on_delete=models.SET_NULL, null=True,
        verbose_name=_('grade')
    )
    departments = models.ManyToManyField(
        Department,
        verbose_name=_('departments')
    )
    dedication = models.ForeignKey(
        Dedication, on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name=_('dedication')
    )
    condition = models.ForeignKey(
        Condition, on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name=_('condition')
    )
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name=_('category')
    )

    def __str__(self):
        return '{self.last_name}, {self.first_name}'.format(self=self)

    def save(self, *args, **kwargs):
        if self.photo:
            image = Img.open(BytesIO(self.photo.read()))
            output = BytesIO()
            image_resized = image.resize((210, 280))
            image_resized.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.photo = InMemoryUploadedFile(output, 'ImageField',
                                              '{}.jpg'.format(self.code),
                                              'image/jpeg',
                                              sys.getsizeof(output), None)
        super(Teacher, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('teacher')
        verbose_name_plural = _('teachers')
        ordering = ('last_name', 'first_name',)


class HourType(models.Model):
    abbreviation = models.SlugField(_('abbreviation'), max_length=8,
                                    unique=True)
    name = models.CharField(_('name'), max_length=14)

    def __str__(self):
        return str(self.abbreviation)

    class Meta:
        verbose_name = _('hour type')
        verbose_name_plural = _('hour types')


class Location(models.Model):
    days_of_week = (
        (1, _('monday')),
        (2, _('tuesday')),
        (3, _('wednesday')),
        (4, _('thursday')),
        (5, _('friday')),
        (6, _('saturday')),
    )
    week_day = models.PositiveSmallIntegerField(_('day of week'),
                                                choices=days_of_week)
    begin_hour = models.PositiveSmallIntegerField(
        _('begin hour'),
        validators=(
            MinValueValidator(8),
            MaxValueValidator(21),
        ))
    end_hour = models.PositiveSmallIntegerField(
        _('end hour'),
        validators=(
            MinValueValidator(8),
            MaxValueValidator(21),
        ))
    hour_type = models.ForeignKey(
        HourType, on_delete=models.SET_NULL, null=True,
        verbose_name=_('hour type')
    )
    classrooms = models.ManyToManyField(
        Classroom,
        verbose_name=_('classrooms')
    )
    teachers = models.ManyToManyField(
        Teacher,
        verbose_name=_('teachers'),
        #limit_choices_to={'departments__acronym': 'DACB'}
    )
    section = models.ForeignKey(Section, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('location')
        verbose_name_plural = _('locations')
        unique_together = (
            ('week_day', 'begin_hour', 'end_hour', 'hour_type', 'section'),
        )


class NewsletterType(models.Model):
    name = models.CharField(max_length=62)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = _('newsletter type')
        verbose_name_plural = _('newsletter types')


class Newsletter(models.Model):
    code = models.SlugField(_('code'), max_length=14, unique=True)
    title = models.CharField(_('title'), max_length=126)
    pub_date = models.DateField(_('date published'))
    newsletter_type = models.ForeignKey(NewsletterType,
                                        on_delete=models.SET_NULL, null=True)
    file = models.FileField(upload_to='newsletters')

    class Meta:
        verbose_name = _('newsletter')
        verbose_name_plural = _('newsletters')


class Announcement(models.Model):
    code = models.SlugField(_('code'), max_length=14, unique=True)
    title = models.CharField(_('title'), max_length=126)
    pub_date = models.DateField(_('date published'))
    file = models.FileField(upload_to='announcements')

    class Meta:
        verbose_name = _('announcement')
        verbose_name_plural = _('announcements')
