/// <reference path="../index.d.ts" />

import * as $ from 'jquery';
import 'bootstrap';
import * as React from 'react';

interface CruceTable extends React.Props<any> {
  title: string;
  cruces: [Location, Location][];
  href: string;
}

export const CruceTable = ({title, cruces, href}: CruceTable) => {
  React.useEffect(() => {
    $('[data-toggle="popover"]').popover();
  }, []);

  return (
    <table className="table table-sm table-bordered text-center table-schedule">
      <thead className="thead-dark">
        <tr>
          <th colSpan={7}>
            <a target="_blank" className="text-white" href={href}>
              {title}
            </a>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>
            Nº Cruce
          </th>
          <th>
            Curso
          </th>
          <th>
            Sección
          </th>
          <th>
            Tipo
          </th>
          <th>
            Horario
          </th>
        </tr>
        {cruces.map(([l1, l2]: [Location, Location], i) =>
          <React.Fragment key={`${l1.id}${l2.id}`}>
            <tr>
              <th scope="row" rowSpan={2} className="align-middle">
                {i + 1}
              </th>
              <td>
                {l1.section.course.code}
              </td>
              <td>
                {l1.section.section}
              </td>
              <td>
                {l1.hour_type.abbreviation}
              </td>
              <td>
                {`${l1.begin_hour}-${l1.end_hour}`}
              </td>
            </tr>
            <tr>
              <td>
                {l2.section.course.code}
              </td>
              <td>
                {l2.section.section}
              </td>
              <td>
                {l2.hour_type.abbreviation}
              </td>
              <td>
                {`${l2.begin_hour}-${l2.end_hour}`}
              </td>
            </tr>
          </React.Fragment>
        )}
      </tbody>
    </table>
  );
}
