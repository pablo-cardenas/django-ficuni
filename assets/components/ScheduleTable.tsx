/// <reference path="../index.d.ts" />

import * as $ from 'jquery';
import 'bootstrap';
import * as React from 'react';

declare const pathTeacherDetail: string;

interface ButtonProps extends React.Props<any> {
  location: Location;
}

const Button = ({location}: ButtonProps) => {
  return (
          <a
            key={location.id}
            tabIndex={0}
            className="btn btn-sm btn-primary"
            role="button"
            data-toggle="popover"
            data-trigger="focus"
            data-html="true"
            title={location.section.course.name}
            data-content={
              location.teachers.map((t: Teacher) => {
                return `
                  <a
                    target="_blank"
                    href=${pathTeacherDetail}/${t.id}>
                    ${t.grade || ''} ${t.last_name}, ${t.first_name}
                  </a>
                `;
              }).join('<br/>') +
              `<br/>Aula: ${location.classrooms.map((c: Classroom) => c.code).join(',')}`
            }>
            {location.section.course.code}-{location.hour_type.abbreviation}
          </a>
  );
}

interface ScheduleTableProps extends React.Props<any> {
  title: string;
  location_set: Location[];
  href: string;
}

export const ScheduleTable = ({title, location_set, href}: ScheduleTableProps) => {
    React.useEffect(() => {
      $('[data-toggle="popover"]').popover();
    }, []);

    const schedule: ButtonProps[][][] = new Array<Array<Array<ButtonProps>>>();
    for (let i = 8; i < 22; i++) {
      schedule[i] = new Array<Array<ButtonProps>>();
      for (let j = 1; j <= 6; j++)
        schedule[i][j] = new Array<ButtonProps>()
    }

    // Fill schedule
    location_set.forEach((location: Location) =>{
      const wd = location.week_day;
      for (let h = location.begin_hour; h < location.end_hour; h++)
        schedule[h][wd].push({location});
    })

  return (
    <table className="table table-sm table-bordered text-center table-schedule">
      <colgroup>
         <col span={1} style={{width: '7%'}}/>
         <col span={1} style={{width: '15.5%'}}/>
         <col span={1} style={{width: '15.5%'}}/>
         <col span={1} style={{width: '15.5%'}}/>
         <col span={1} style={{width: '15.5%'}}/>
         <col span={1} style={{width: '15.5%'}}/>
         <col span={1} style={{width: '15.5%'}}/>
      </colgroup>
      <thead className="thead-dark">
        <tr>
          <th colSpan={7} className="text-center">
            <a target="_blank" className="text-white" href={href}>{title}</a>
          </th>
        </tr>
      </thead>
      <thead>
        <tr>
          <th scope="col"></th>
          <th scope="col">Lun</th>
          <th scope="col">Mar</th>
          <th scope="col">Mié</th>
          <th scope="col">Jue</th>
          <th scope="col">Vie</th>
          <th scope="col">Sáb</th>
        </tr>
      </thead>
      <tbody>
        {
          // TODO: Refactor
          [8,9,10,11,12,13,14,15,16,17,18,19,20,21].map((h: number) => (
            <tr key={h}>
              <th scope="row" className="align-middle">
                {String((h-1)%12+1).padStart(2, '0')}-{String(h%12+1).padStart(2, '0')}
              </th>
              {
                // TODO: Refactor
                [1,2,3,4,5,6].map((wd: number) => (
                  <td key={wd} className="align-middle">
                    {schedule[h][wd].map((buttonProps: ButtonProps) => 
                      <Button key={`${buttonProps.location.id}`} {...buttonProps}/>
                    )}
                  </td>
                ))
              }
            </tr>
          ))
        }
      </tbody>
    </table>
  );
}
