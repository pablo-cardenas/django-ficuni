import './index.scss';
import './base.ts';

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import * as $ from 'jquery';
import 'bootstrap';

$('.carousel').carousel({
  pause: false,
  interval: 6000
});

const navbar:HTMLElement = document.getElementsByTagName('nav')[0] as HTMLElement;
const previousColor = navbar.style.backgroundColor;

interface TransparentNavProps { scrollStart: number; }
const TransparentNav = ({scrollStart}: TransparentNavProps) => {
  const [transparent, setTransparent] = React.useState(window.scrollY <= scrollStart);

  // Add event to change navbar color if scrolled
  document.addEventListener('scroll', (e) => {
    const shouldBeTransparent = window.scrollY <= scrollStart;
    if (shouldBeTransparent !== transparent) {
      setTransparent(shouldBeTransparent);
    }
  });

  React.useEffect(() => {
    console.log(previousColor);
    navbar.style.backgroundColor = transparent ? '#00000000' : previousColor;
  } , [transparent])

  return <></>;
}

ReactDOM.render(
  <TransparentNav scrollStart={64} />,
  document.body.appendChild(document.createElement('div'))
);

declare const images: string[];
const carousel_img:HTMLCollectionOf<HTMLDivElement> = document.getElementsByClassName('carousel-img') as HTMLCollectionOf<HTMLDivElement>;
for (let i = 0; i < images.length; i++) {
    carousel_img[i].style.backgroundImage = 'url(' + images[i] + ')' as string;
}

