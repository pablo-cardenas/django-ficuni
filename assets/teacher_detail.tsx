import * as React from 'react';
import * as ReactDOM from 'react-dom';

import './base';
import './teacher_detail.scss';
import './schedule.scss';
import {ScheduleTable} from './components/ScheduleTable';

declare const teacherId: string;
declare const pathTeacherDetail: string;
declare const pathTeacherJson: string;

interface TeacherTableProps extends React.Props<any> {
  teacherId: number;
}

const TeacherTable = ({teacherId}: TeacherTableProps) => {
  const [teacherDetail, setTeacherDetail] = React.useState(null as TeacherDetail);

  React.useEffect(() =>{
    fetch(`${pathTeacherJson}/${teacherId}`).then(async (response) => {
      setTeacherDetail(await response.json());
    })
  }, []);

  if (teacherDetail === null)
    return <div>Descargando secciones...</div>;

  return (
    <ScheduleTable
      location_set={teacherDetail.location_set}
      title={teacherDetail.code}
      href={`${pathTeacherDetail}/${teacherDetail.code}`}
    />
  );
}

ReactDOM.render(
  <TeacherTable teacherId={Number(teacherId)} />,
  document.getElementById('root')
);
