declare interface Course {
  id: number;
  code: string;
  name: string;
  cycle: number;
}

declare interface EvaluationSystem {
  code: string;
}

declare interface Department {
  acronym: string;
}

declare interface HourType {
  abbreviation: string;
}

declare interface Classroom {
  code: string;
}

interface Teacher {
  id: number;
  code: string;
  first_name: string;
  last_name: string;
  grade: string;
}

interface TeacherDetail {
  id: number;
  code: string;
  first_name: string;
  last_name: string;
  grade: string;
  location_set: Location[];
}

declare interface Section {
  course: Course;
  section: string;
}

declare interface Location {
  id: number;
  section: Section;
  week_day: number;
  begin_hour: number;
  end_hour: number;
  hour_type: HourType;
  classrooms: Classroom[];
  teachers: Teacher[];
}

declare interface SectionDetail {
  course: Course;
  section: string;
  location_set: Location[];
}

declare interface CourseDetail {
  id: number;
  code: string;
  name: string;
  cycle: number;
  num_of_credits: number;
  evaluation_system: EvaluationSystem;
  department: Department;
  section_set: SectionDetail[];
}


