/// <reference path="../index.d.ts" />

import * as React from 'react';

interface FormShowCrucesProps {
  showCruces: number;
  setShowCruces: React.Dispatch<React.SetStateAction<number>>;
  categories: any;
}

export function FormShowCruces({showCruces, setShowCruces, categories}: FormShowCrucesProps) {
  const a: string[] = ['sin cruces', 'con un cruce permitido', 'con dos cruces permitidos', 'no permitidos'];
      return <div className="row"> {
    a.map((v: string, i: number) =>
      <div className="my-2 col-lg-12 col-md-6">
        <div className="form-check" key={i}>
          <input
            className="form-check-input"
            id={`mostrar_${i}_cruces`}
            type="checkbox"
            checked={Boolean(showCruces === i)}
            onChange={() => setShowCruces(i)} />
          <label
            className="form-check-label"
            htmlFor={`mostrar_${i}cruces`}>
            {categories[i].length} horario {v}
          </label>
        </div>
      </div>
    )
  } </div>
}
