/// <reference path="../index.d.ts" />

import * as React from 'react';

import {CruceTable} from '../components/CruceTable';
import {ScheduleTable} from '../components/ScheduleTable';
import {get_cruces, flatten} from './utils';

declare const pathShareSchedule: string;

interface OutcomesProps{
  showCruces: number;
  categories: any; //TODO: Change any
}

export function Outcomes({categories, showCruces}: OutcomesProps) {
  return <> {
    categories[showCruces].map((m: [CourseDetail, SectionDetail][]) => {
      const queryString = m.map(([course, section]) =>
        `${course.code}=${section.section}`
      ).join('&');

      const title = m.map(([course, section]) =>
        `${course.code}${section.section}`
      ).join(' - ');

      const key = m.map(([course, section]) =>
        `${course.code}${section.section}`
      ).join('_');

      if (showCruces === 3) {
        const cruces = get_cruces(m);
        return (
          <CruceTable
            key={key}
            cruces={cruces}
            title={title}
            href={`${pathShareSchedule}?${queryString}`}
          />
        );
      }
      else
      {
        return (
          <ScheduleTable
            key={key}
            location_set={flatten(m.map(([c, s]: [CourseDetail, SectionDetail]): Location[] => s.location_set))}
            title={title}
            href={`${pathShareSchedule}?${queryString}`}
          />
        );
      }
    })
  } </>
}
