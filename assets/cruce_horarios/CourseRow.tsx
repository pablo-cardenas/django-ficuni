/// <reference path="../index.d.ts" />

import * as React from 'react';

declare const pathCourseDetail: string;
declare const pathCourseList: string;
declare const pathDepartmentDetail: string;

interface CourseRowProps {
  courseDetail: CourseDetail;
  selectedSections: Set<Section>;
  onChangeSection: (section: Section) => void;
  onRemoveCourse: (courseDetail: CourseDetail) => void;
}

export const CourseRow = ({courseDetail, selectedSections, onChangeSection, onRemoveCourse}: CourseRowProps) => {
  return (
    <tr className="align-middle">
      <th>
        <a
          target="_blank"
          href={`${pathCourseDetail}/${courseDetail.code}`}>
          {courseDetail.code}
        </a>
      </th>
      <td>
        <a
          target="_blank"
          href={`${pathCourseDetail}/${courseDetail.code}`}>
          {courseDetail.name}
        </a>
      </td>
      <td>
        {courseDetail.section_set.map((section: Section) => {
          const id = `checkbox_${section.course.code}_${section.section}`;
          return (
            <div
              key={section.section}
              className="form-check form-check-inline">
              <input
                className="form-check-input"
                id={id}
                type="checkbox"
                value={section.section}
                defaultChecked={selectedSections.has(section)}
                onChange={() => onChangeSection(section)} />
              <label
                className="form-check-label"
                htmlFor={id}>
                {section.section}
              </label>
            </div>
          );
        })}
      </td>
      <td>{courseDetail.num_of_credits}</td>
      <td>
        <a
          target="_blank"
          href={ courseDetail.cycle == 11 ?
            `${pathCourseList}/electivos/` :
            `${pathCourseList}/ciclo-${courseDetail.cycle}/`
          }>
          {courseDetail.cycle}
        </a>
      </td>
      <td>
        <a
          target="_blank"
          href={`${pathDepartmentDetail}/${courseDetail.department.acronym}`}>
          {courseDetail.department.acronym}
        </a>
      </td>
      <td>
        <button className="btn btn-outline-danger btn-sm" type="button"
                  onClick={() => onRemoveCourse(courseDetail)}>
          <span className="fa fa-trash"></span>
        </button>
      </td>
    </tr>
  );
}
