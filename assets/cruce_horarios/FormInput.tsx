/// <reference path="../index.d.ts" />

import * as React from 'react';

interface FormInput {
  courseList: Course[];
  addCourse: (course: Course) => void;
}

export function FormInput({courseList, addCourse}: FormInput) {
  const [input, setInput] = React.useState('');
  return <>
    <input
      type="text"
      list="dl_courses"
      className="form-control"
      id="input_courses"
      placeholder="ciclo) código: Nombre del curso"
      value={input}
      onChange={(e: any) => setInput(e.target.value) }
      pattern={courseList.map(course => course.code).join('|')}
    />
    <div className="input-group-append">
      <button
        className="btn btn-primary"
        type="button"
        id="btn_add"
        onClick={ () => {
          addCourse(courseList.find(course => course.code == input))
          setInput('');
        }} >
        <span className="fa fa-plus"></span>
      </button>
    </div>
  </>
}
