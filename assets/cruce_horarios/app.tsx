/// <reference path="../index.d.ts" />

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
//import '@fortawesome/fontawesome-free/js/regular'
//import '@fortawesome/fontawesome-free/js/brands'

import '../base';
import '../schedule.scss';
import {ScheduleTable} from '../components/ScheduleTable';
import {CruceTable} from '../components/CruceTable';
import {FormShowCruces} from './FormShowCruces';
import {FormTable} from './FormTable';
import {Outcomes} from './Outcomes';
import {get_cruces, flatten} from './utils';

declare const pathCourseJson: string;

function useSelectedCourses() {
  const [selectedCourses, setSelectedCourses] = React.useState(new Map<CourseDetail, Set<SectionDetail>>());
  const [error, setError] = React.useState(null);

  function addCourse(course: Course) {
    if (!course) {
      return
    }
    fetch(`${pathCourseJson}/${course.id}`)
      .then(res => res.json())
      .then(
        (result: CourseDetail) => {
          const newSelectedCourses = new Map(selectedCourses);
          newSelectedCourses.set(result, new Set);
          setSelectedCourses(newSelectedCourses);
        },
        (error) => {
          setError(error);
        }
      )
  }

  function handleChangeSection(courseDetail: CourseDetail, section: SectionDetail) {
    const newSelectedCourses = new Map(selectedCourses);

    if (!selectedCourses.has(courseDetail))
      return;
    if (selectedCourses.get(courseDetail).has(section))
      newSelectedCourses.get(courseDetail).delete(section);
    else
      newSelectedCourses.get(courseDetail).add(section);

    setSelectedCourses(newSelectedCourses);
  }

  function handleRemoveCourse(courseDetail: CourseDetail) {
    const newSelectedCourses = new Map(selectedCourses);
    if (!newSelectedCourses.has(courseDetail))
      return;
    newSelectedCourses.delete(courseDetail)
    setSelectedCourses(newSelectedCourses);
  }

  return {selectedCourses, addCourse, handleRemoveCourse, handleChangeSection};
}

function useShowCruces(initial: number) {
  const [showCruces, setShowCruces] = React.useState(initial);
  return {showCruces, setShowCruces};
}

const App = (props: any) => {
  const _selectedCourses = useSelectedCourses();
  const [courseList, setCourseList] = React.useState(null as Course[]);
  const _showCruces = useShowCruces(0);
  const [categories, setCategories] = React.useState([[], [], [], []]);

  React.useEffect(() => {
    fetch(pathCourseJson).then(async (response) =>{
      setCourseList(await response.json());
    });
  }, []);

  React.useEffect(() => {
    function cartesian_product(pools: [CourseDetail, Set<SectionDetail>][]): [CourseDetail, SectionDetail][][] {
      return pools.reduce((acc: [CourseDetail, SectionDetail][][], [course, sections]: [CourseDetail, Set<SectionDetail>]): [CourseDetail, SectionDetail][][] =>
        flatten(acc.map((x: [CourseDetail, SectionDetail][]): [CourseDetail, SectionDetail][][] =>
          Array.from(sections.values()).map((section: SectionDetail): [CourseDetail, SectionDetail][] => [...x, [course, section]])
      )), [new Array<[CourseDetail, SectionDetail]>()])
    }


    function categorize(cruces: [Location, Location][]) {
      const forbiddenTypes = new Set(['PR', 'TA', 'LAB']);
      const hasForbidenCruces = cruces.some(pair => pair.every((l: Location) => forbiddenTypes.has(l.hour_type.abbreviation)));

      if (cruces.length > 2 || hasForbidenCruces)
        return 3
      else
        return cruces.length;
    }

    const cp = cartesian_product(Array.from(_selectedCourses.selectedCourses));

    setCategories([
      cp.filter((m) => { const cruces = get_cruces(m); return categorize(cruces) == 0 }),
      cp.filter((m) => { const cruces = get_cruces(m); return categorize(cruces) == 1 }),
      cp.filter((m) => { const cruces = get_cruces(m); return categorize(cruces) == 2 }),
      cp.filter((m) => { const cruces = get_cruces(m); return categorize(cruces) == 3 }),
    ]);
  }, [_selectedCourses.selectedCourses]);

  if (courseList === null) {
    return <div>Descargando cursos...</div>;
  }

  return (
    <div className="row">
      <div className="col-lg-9">
        <FormTable {..._selectedCourses} {..._showCruces} courseList={courseList}/>
      </div>
      <div className="col-lg-3">
        <FormShowCruces {..._showCruces} categories={categories} />
      </div>
      <div className="col-lg-9">
        <Outcomes categories={categories} showCruces={_showCruces.showCruces} />
      </div>
    </div>
  );
}

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);
