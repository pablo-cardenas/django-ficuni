/// <reference path="../index.d.ts" />

import * as React from 'react';

import {CourseRow} from './CourseRow';
import {FormInput} from './FormInput';

interface FormTableProps {
  selectedCourses: Map<CourseDetail, Set<Section>>;
  handleChangeSection: (courseDetail: CourseDetail, section: Section) => void;
  handleRemoveCourse: (courseDetail: CourseDetail) => void;
  courseList: Course[];
  addCourse: (c: Course) => void;
  showCruces: number;
  setShowCruces: React.Dispatch<React.SetStateAction<number>>;
}

export function FormTable({selectedCourses, handleChangeSection, handleRemoveCourse, courseList, addCourse, showCruces, setShowCruces}: FormTableProps) {

  const cycles = Array.from(selectedCourses.keys()).map((course) =>
    course.cycle
  );
  const cycleSet = new Set(cycles)
  //return '|{' + [...cycleSet].join(',') + '}|=' + cycleSet.size;
  const numDifferentCycles = cycleSet.size;

  const numCredits = Array.from(selectedCourses.keys()).reduce((acc, course) =>
    acc + course.num_of_credits,
    0
  );

  const numSections = Array.from(selectedCourses.values()).map(sections =>
    sections.size
  );
  const product = numSections.reduce((acc, ns) =>
    acc * ns,
    1
  );
  const numSchedules = numSections.join('×') + " = " + product;

  return <>
    <label htmlFor="input_courses">Código del curso</label>
    <div className="input-group mb-3">
      <FormInput addCourse={addCourse} courseList={courseList}/>
    </div>
    <datalist id="dl_courses">
    {
      Array.from(courseList.values()).map(course =>
        <option key={course.code} value={course.code}>
          {`${course.cycle}) ${course.code}: ${course.name}`}
        </option>
      )
    }
    </datalist>
    <div className="table-responsive">
      <table className="table table-sm">
        <thead className="thead-dark align-middle">
          <tr>
            <th scope="col">Código</th>
            <th scope="col">Curso</th>
            <th scope="col">Secciones</th>
            <th scope="col">Créd.</th>
            <th scope="col">Ciclo</th>
            <th scope="col">Dept.</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
        {
          Array.from(selectedCourses).map(([courseDetail, selectedSections]: [CourseDetail, Set<Section>]) =>
            <CourseRow
              key={courseDetail.code}
              courseDetail={courseDetail}
              selectedSections={selectedSections}
              onChangeSection={section => handleChangeSection(courseDetail, section)}
              onRemoveCourse={section => handleRemoveCourse(courseDetail)} />
          )
        }
        </tbody>
        <tfoot>
          <tr>
            <th scope="col"></th>
            <th scope="col">{selectedCourses.size} cursos</th>
            <th scope="col">{numSchedules}</th>
            <th scope="col">{numCredits}</th>
            <th scope="col">{numDifferentCycles}</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </>;
}
