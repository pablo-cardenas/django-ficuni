/// <reference path="../index.d.ts">

export function get_cruces(m: [CourseDetail, SectionDetail][]): [Location, Location][] {
  function get_cruces_locations(locations1: Location[], locations2: Location[]): [Location, Location][] {
    // Cartesian product
    const cp = flatten(locations1.map((l1: Location): [Location, Location][] =>
      locations2.map((l2: Location): [Location, Location] =>
        [l1, l2]
    )));

    return cp.filter(([l1, l2]: [Location, Location]) =>
      l1.week_day == l2.week_day && l1.end_hour > l2.begin_hour && l2.end_hour > l1.begin_hour
    );
  }

  function pairs([x, ...xs]: [CourseDetail, SectionDetail][]) : [[CourseDetail, SectionDetail], [CourseDetail, SectionDetail]][] {
    if (x === undefined)
      return [];
    return xs.map((y: [CourseDetail, SectionDetail]): [[CourseDetail, SectionDetail], [CourseDetail, SectionDetail]] =>
      [y, x]).concat(pairs(xs));
  }

  return  flatten(pairs(m).map(([[c1, s1], [c2, s2]]: [[CourseDetail, SectionDetail], [CourseDetail, SectionDetail]]) =>
    get_cruces_locations(s1.location_set, s2.location_set)
  ));
}

export function flatten<T>(arr: T[][]): T[] {
  return [].concat.apply([], arr)
}

