'use strict'

import * as $ from 'jquery';
import 'bootstrap';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import '../base';
import '../schedule.scss';
import {ScheduleTable} from "../components/ScheduleTable";


declare const pathLocationJson: string;

declare class URLSearchParams {
    /** Constructor returning a URLSearchParams object. */
    constructor(init?: string| URLSearchParams);

    /** Returns an iterator allowing to go through all key/value pairs contained in this object. */
    entries(): IterableIterator<[string, string]>;
}

function App() {
  const searchParams = new URLSearchParams(window.location.search);
  const [locations, setLocations] = React.useState(new Array<Location>());
  const [error, setError] = React.useState(null);
  const [isLoaded, setIsLoaded] = React.useState(null);


  React.useEffect(() => {
    for (const [course_code, section] of Array.from(searchParams.entries()))
    {
      fetch(`${pathLocationJson}/${window.location.search}`)
        .then(res => res.json())
        .then(
          (result) => {
            setLocations(result);
            setIsLoaded(true);
            $('[data-toggle="popover"]').popover();
          },
          (error) => {
            setError(error);
            setIsLoaded(true);
          }
        )
    }
  }, []);

  if (error)
    return <div>Error al descargar secciones: {error.message}</div>;
  if (!isLoaded)
    return <div>Cargando...</div>;

  const title = (
    Array.from(searchParams.entries()).map(([course_code, section]: [string, string]) => `${course_code}${section}`).join(' - ')
  );

  return <ScheduleTable
    location_set={locations}
    title={title}
    href={window.location.search}
  />
}


ReactDOM.render(
  <App />,
  document.getElementById('root')
);
