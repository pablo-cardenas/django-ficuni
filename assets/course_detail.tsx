/// <reference path="index.d.ts" />

import './base.ts';
import './schedule.scss';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import {ScheduleTable} from "./components/ScheduleTable";

declare const course_id: number;
declare const pathCourseJson: string;

interface CourseDetailAppProps { courseId: number; }
const CourseDetailApp = ({courseId}: CourseDetailAppProps) => {
  const [courseDetail, setCourseDetail] = React.useState(null as CourseDetail);

  React.useEffect(() => {
    fetch(`${pathCourseJson}/${courseId}`).then(async (response) => {
      setCourseDetail(await response.json());
    })
  }, []);


  if (courseDetail === null) {
    return <div>Descargando secciones...</div>;
  }

  return <> {
    courseDetail.section_set.map((section: SectionDetail) =>{
      return (
      <ScheduleTable
        key={section.section}
        location_set={section.location_set}
        title={`${section.course.code}${section.section}`}
        href={'#'}
      />
      )}
    )
  } </>;
}

ReactDOM.render(
  <CourseDetailApp courseId={course_id}/>,
  document.getElementById('root')
);
